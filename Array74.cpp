#include <iostream>
#include <cstdlib>
#include <vector>
#include <ctime>

std::vector<int> randomArray (int min, int max, int a)
{
	std::vector<int> A;
	for (int i = 0; i < a; i++)
	{
		int x = min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
		A.resize(i+1);
		A[i] = x;
	}
	return A;
}
int min (std::vector<int> A)
{
	int min = A[0];
	int q;
	srand(static_cast<unsigned int>(time(0)));
	for (q = 0; q < A.size(); q++)
	{
		if (A[q] < min) min = A[q];
	}
	int i;
	for (i = 0; i < A.size(); i++)
	{
		if (A[i] == min) break;
	}
	return i;
}
int max (std::vector<int> A)
{
	int max = A[0];
	int q;
	srand(static_cast<unsigned int>(time(0)));
	for (q = 0; q < A.size(); q++)
	
	
		if (A[q] > max) max = A[q];
	int i;
	for (i = 0; i < A.size(); i++)
	{
		if (A[i] == max) break;
	}
	return i;
}
int main()
{
	int N;
	const int minValue = -1000000;
	const int maxValue = 1000000;
	
	std::cout << "Enter the size of the array: ";
	std::cin >> N;
	std::vector<int> A = randomArray(minValue, maxValue, N);
	for (int i = 0; i < N; i++)
	{
		std::cout << A[i] << std::endl;
	}
	std::cout << "The end of the array. " << std::endl;
	int a = min(A);
	
	int b = max(A);
	
	if (a > b)
	{
		for (int i = b+1; i < a; i++)
		{
			A[i] = 0;	
		}	
	} 
	if (b > a)
	{
		for (int i = a+1; i < b; i++)
		{
			A[i] = 0;
		}
	}
	for (int i = 0; i < N; i++)
	{
		std::cout << A[i] << std::endl;
	}
	return 0;
}