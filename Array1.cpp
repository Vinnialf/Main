#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

std::vector <int> getArray (int min, int max, int a)
{
	std::vector <int> A;
	for (int i = 0; i < a; i++)
	{
		int b, x;
		x = A.size();
		b = min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min - 1));
		A. resize(x+1);
		A[x] = b;
	}
	return A;
}

int main()
{
	std::cout << "Please, enter the size of the array: ";
	int a;
	std::cin >> a;
	std::vector <int> A = getArray(-1000000, 1000000, a);
	for (int i = 0; i < a; i++)
	{
		std::cout << A[i] << std::endl;
	}
	return 0;
}