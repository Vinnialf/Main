#include <iostream>
#include <cmath>

using namespace std;
int factorial(int N)
{
	
	int res = 1;
	if (N % 2 == 0)
		
	{
		int i = 2;
		while (i <= N)
		{
			res = res * i;
			i = i + 2;
		}
	}
	else
	{
		int i = 1;
		while (i <= N)
		{
			res = res * i;
			i = i + 2;
		}
			
	}
	return res;
}
int main()
{
	int N;
	cin >> N;
	cout << factorial(N);
	return 0;
}
