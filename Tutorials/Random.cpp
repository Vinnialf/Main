#include <iostream>
#include <cstdlib>
#include <ctime>

//Ця програма генерує псевдовипадкові числа у заданому діапазоні без використання ООП. Зауважте, він не є досконалим,
//оскільки не забезпечує рівномірного розподілу чисел. Більш досконалим варіантом є використання вихору Мерсенна, але
//цей спосіб вимагає використання ООП.

int getRandomNumber(int min, int max)
{
    return min + static_cast<int>(static_cast<double>(rand()) / RAND_MAX * (max - min + 1));
}

int main()
{
    srand(static_cast<unsigned int>(time(0)));

    const int minValue = -1000000;
    const int maxValue = 1000000;
    const int numNumbers = 100;

    for (int i = 0; i < numNumbers; i++)
    {
        int randomNumber = getRandomNumber(minValue, maxValue);
        std::cout << randomNumber << std::endl;
    }

    return 0;
}