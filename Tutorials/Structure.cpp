#include <iostream> 
#include <string> 
 
struct Employee 
{ 
 std::string name; 
 int salary; 
 short age; 
}; 
struct Company 
{ 
 Employee CEO; 
 int number_of_employees; 
}; 
int main() 
{ 
 Company company; 
 getline(std::cin, company.CEO.name); 
 std::cin >> company.CEO.salary; 
 std::cin >> company.CEO.age; 
 std::cin >> company.number_of_employees; 
 
 std::cout << company.CEO.name << std::endl; 
 std::cout << company.CEO.salary << std::endl; 
 std::cout << company.CEO.age << std::endl; 
 std::cout << company.number_of_employees << std::endl; 
 return 0; 
} 
//Ця програма показує, як працювати зі структурами в С++.