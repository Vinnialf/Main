#include <iostream>
#include <vector>

std::vector <int> getArray (int N)
{
	std::vector<int> A;
	int x = 0;
	for (int i = 0; i < N; i++)
	{
		int m;
		std::cout << "Element #" << i << ": ";
		std::cin >> m;
		A.resize(x + 1);
		A[x] = m;
		x++;
	}
	return A;
}

std::vector <int> modifyArray(std::vector<int>A )
{
	for (int i = 0; i < A.size(); i++)
	{
		if (A[i] < A[i+1]) 
		{
			std::swap(A[i], A[i+1]); 
			break;	
		}
	}
		
	return A;	
}	
int main()
{
	int N;
	std::cout << "Enter the size of the array: ";
	std::cin >> N;
	std::vector<int> A = getArray(N); 
	A = modifyArray(A);
	for (int i = 0; i < A.size(); i++)
	{
		std::cout << A[i] << std::endl;
	}
	return 0;
}